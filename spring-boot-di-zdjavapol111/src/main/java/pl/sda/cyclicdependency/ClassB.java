package pl.sda.cyclicdependency;

import org.springframework.stereotype.Component;

@Component
public class ClassB {

    private final ClassCommon classCommon;

    public ClassB(ClassCommon classCommon) {
        this.classCommon = classCommon;
    }
}
