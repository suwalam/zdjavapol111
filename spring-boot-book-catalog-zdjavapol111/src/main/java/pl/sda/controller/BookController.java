package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.model.Book;
import pl.sda.service.BookService;

import javax.validation.Valid;

@Slf4j
@Controller
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping(path = "/books")
    public String bookList(ModelMap modelMap) {
        modelMap.addAttribute("books", bookService.getAllSortedByTitle());

        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        modelMap.addAttribute("currentUser", currentUser);

        return "book-list";
    }

    @GetMapping(path = "/books/page/{pageNumber}")
    public String bookListByPage(@PathVariable int pageNumber, ModelMap modelMap) {
        modelMap.addAttribute("books", bookService.getAllForPageNumber(pageNumber));

        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        modelMap.addAttribute("currentUser", currentUser);

        return "book-list";
    }

    @GetMapping(path = "/books/{isbn}")
    public String bookDetails(@PathVariable String isbn, ModelMap modelMap) {
        modelMap.addAttribute("book", bookService.getByIsbn(isbn));
        return "book-details";
    }

    @GetMapping(path = "/admin/create")
    public String showCreateBookForm(ModelMap modelMap) {
        modelMap.addAttribute("emptyBook", new Book());
        return "book-create";
    }

    @PostMapping(path = "/admin/save")
    public String handleNewBook(@ModelAttribute("emptyBook") Book book) {
        log.info("Handle new book: " + book);

        bookService.save(book);
        return "redirect:/books";
    }

    @GetMapping(path = "/edit/{id}")
    public String showUpdateBookForm(@PathVariable Integer id, ModelMap modelMap) {
        modelMap.addAttribute("book", bookService.getById(id));
        return "book-edit";
    }

    @PostMapping(path = "/update")
    public String handleUpdatedBook(@ModelAttribute("book") Book book) {
        log.info("Handle updated book: " + book);
        bookService.update(book);
        return "redirect:/books";
    }

    @GetMapping(path = "/delete/{id}")
    public String deleteBook(@PathVariable Integer id) {
        log.info("Delete book with id " + id);
        bookService.deleteById(id);
        return "redirect:/books";
    }

}
