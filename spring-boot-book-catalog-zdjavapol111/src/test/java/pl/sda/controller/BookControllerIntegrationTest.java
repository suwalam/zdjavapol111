package pl.sda.controller;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.sda.model.Book;

import java.time.LocalDate;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class BookControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private Book bookTest = new Book(1, "Pan Tadeusz", "Adam Mickiewicz",
            "12345", "opis", LocalDate.now().minusYears(130));


    @WithMockUser(username = "user", password = "pass", roles = "USER")
    @Test
    public void shouldReturnBookList() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/books"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("books"))
                .andExpect(MockMvcResultMatchers.model().attribute("books", Matchers.notNullValue()))
                .andExpect(MockMvcResultMatchers.view().name("book-list"));
    }

    @WithMockUser(username = "admin", password = "pass", roles = "ADMIN")
    @Test
    public void saveBookTest() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders
                .post("/admin/save")
                .param("id", Integer.toString(bookTest.getId()))
                .param("title", bookTest.getTitle())
                .param("author", bookTest.getAuthor())
                .param("isbn", bookTest.getIsbn())
                .param("description", bookTest.getDescription())
                .param("releaseDate", bookTest.getReleaseDate().toString())
        ) //zakończenie wywołania metody perform
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.view().name("redirect:/books"))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/books"));
    }

}