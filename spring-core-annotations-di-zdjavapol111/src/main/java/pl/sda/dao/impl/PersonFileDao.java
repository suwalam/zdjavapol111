package pl.sda.dao.impl;

import pl.sda.dao.PersonDao;
import pl.sda.model.Person;

import java.util.ArrayList;
import java.util.List;

public class PersonFileDao implements PersonDao {
    @Override
    public Person getById(int id) {
        //symulujemy odczyt z pliku
        return new Person(1, "Jan", "Kowalski");
    }

    @Override
    public List<Person> getAll() {
        //symulujemy odczyt z pliku
        return new ArrayList<>();
    }
}
