package pl.sda.service.impl;

import org.springframework.stereotype.Service;
import pl.sda.model.Message;
import pl.sda.service.MessageService;

import java.util.ArrayList;
import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {

    private List<Message> messages = new ArrayList<>();

    public MessageServiceImpl() {
        messages.add(new Message(1, "message-1"));
        messages.add(new Message(2, "message-2"));
    }

    @Override
    public Message getById(Integer id) {
        for (Message message : messages) {
            if (message.getId().equals(id)) {
                return message;
            }
        }

        return null;
    }

    @Override
    public List<Message> getAll() {
        return messages;
    }

    @Override
    public void add(Message message) {
        messages.add(message);
    }

    @Override
    public void update(Message message) {
        int index = getIndexById(message.getId());

        if (index != -1) {
            messages.get(index).setText(message.getText());
        }
    }

    @Override
    public void deleteById(Integer id) {
        Message message = getById(id);

        if (message != null) {
            messages.remove(message);
        }
    }

    private int getIndexById(Integer id) {
        for (int i=0; i<messages.size(); i++) {
            if (messages.get(i).getId().equals(id)) {
                return i;
            }
        }

        return -1;
    }
}
