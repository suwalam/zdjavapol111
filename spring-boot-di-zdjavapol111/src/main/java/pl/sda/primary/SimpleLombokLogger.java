package pl.sda.primary;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class SimpleLombokLogger implements SimpleLogger{

    @Override
    public void printMessage(String message) {
       log.info("LOMBOK: " + message);
    }
}
