package pl.sda.model;

import lombok.AllArgsConstructor;
import lombok.Data;

//klasa POJO - Plain Old Java Object

@Data
@AllArgsConstructor
public class Person {

    private int id;

    private String name;

    private String surname;

}
