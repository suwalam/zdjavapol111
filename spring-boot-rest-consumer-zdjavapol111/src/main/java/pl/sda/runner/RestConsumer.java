package pl.sda.runner;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.sda.model.Message;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class RestConsumer implements CommandLineRunner {

    private RestTemplate restTemplate = new RestTemplate();

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void run(String... args) throws Exception {

        String getAllURL = "http://localhost:8080/api/messages/";


        final String jsonMessage = restTemplate.getForObject(getAllURL, String.class);
        log.info("Result of call GET request: " + jsonMessage);

        final List<Message> messages = objectMapper.readValue(jsonMessage,
                objectMapper.getTypeFactory().constructCollectionType(List.class, Message.class));

        messages.forEach(m -> log.info("Message: " + m));

        //***********************************************************
        String getByIdURL = "http://localhost:8080/api/messages/{id}";
        Map<String, String> params = new HashMap<>();
        params.put("id", "2");

        final Message messageById = restTemplate.getForObject(getByIdURL, Message.class, params);
        log.info("Message by id: " + messageById);

        final ResponseEntity<Message> responseEntity =
                restTemplate.getForEntity(getByIdURL, Message.class, params);

        log.info("Response Entity status code for get by id: " + responseEntity.getStatusCodeValue());
        log.info("Message from Response Entity: " + responseEntity.getBody());


    }
}
