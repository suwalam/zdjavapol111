package pl.sda.profile;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("dev")
@Component
public class DatabaseDevConnection implements DatabaseConnection {

    @Override
    public String getDatabaseURL() {
        return "DEV URL";
    }
}
