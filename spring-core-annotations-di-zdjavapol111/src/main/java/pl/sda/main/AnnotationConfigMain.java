package pl.sda.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.sda.configuration.PersonConfiguration;
import pl.sda.service.impl.PersonServiceImpl;

public class AnnotationConfigMain {

    public static void main(String[] args) {

        ApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(PersonConfiguration.class);

        final PersonServiceImpl personService =
                applicationContext.getBean("personService", PersonServiceImpl.class);

        System.out.println(personService.getById("1"));

    }

}
