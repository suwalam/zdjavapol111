package pl.sda.cyclicdependency;

import org.springframework.stereotype.Component;

@Component
public class ClassA {

    private final ClassCommon classCommon;

    public ClassA(ClassCommon classCommon) {
        this.classCommon = classCommon;
    }
}
