package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBookCatalogZdjavapol111Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBookCatalogZdjavapol111Application.class, args);
	}

}
