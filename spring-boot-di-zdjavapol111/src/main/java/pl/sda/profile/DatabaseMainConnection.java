package pl.sda.profile;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("main")
@Component
public class DatabaseMainConnection implements DatabaseConnection {
    @Override
    public String getDatabaseURL() {
        return "MAIN URL";
    }
}
