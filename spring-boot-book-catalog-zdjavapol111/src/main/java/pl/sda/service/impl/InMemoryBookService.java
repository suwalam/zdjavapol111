package pl.sda.service.impl;

import org.springframework.stereotype.Service;
import pl.sda.model.Book;
import pl.sda.service.BookService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class InMemoryBookService implements BookService {

    private List<Book> books = new ArrayList<>();

    public InMemoryBookService() {
        books.add(new Book(1, "Pan Tadeusz", "Adam Mickiewicz", "1111", "opis", LocalDate.now().minusYears(170)));
        books.add(new Book(2, "Balladyna", "Juliusz Słowacki", "2222", "opis", LocalDate.now().minusYears(167)));
    }

    @Override
    public void save(Book book) {
        books.add(book);
    }

    @Override
    public Book getByIsbn(String isbn) {
        for(Book book : books) {
            if (book.getIsbn().equals(isbn)) {
                return book;
            }
        }

        return null;
    }

    @Override
    public List<Book> getAll() {
        return books;
    }

    @Override
    public void deleteById(Integer id) {
        //dokończyć w ramach pracy domowej
    }

    @Override
    public void update(Book book) {
        //dokończyć w ramach pracy domowej
    }

    @Override
    public Book getById(Integer id) {
        //dokończyć w ramach pracy domowej
        return null;
    }

    @Override
    public List<Book> getAllSortedByTitle() {
        //dokończyć w ramach pracy domowej
        return null;
    }

    @Override
    public List<Book> getAllForPageNumber(int pageNumber) {
        return null;
    }
}
