package pl.sda.component;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@ConfigurationProperties(prefix = "pl.sda.collections")
@Component
public class ComponentWithPropertiesCollection {

    private List<String> usernames;

    private Map<String, Integer> map;

    private Integer propValue;

    public void printUsernames() {
        usernames.forEach(System.out::println);
    }

    public void printMap() {
        map.forEach((k, v) -> System.out.println("key: " + k + " value: " + v));
    }

    public void printProp() {
        System.out.println("PROP: " + propValue);
    }

    //setter niezbędny do wstrzyknięcia kolekcji z pliku application.properties
    public void setUsernames(List<String> usernames) {
        this.usernames = usernames;
    }

    public void setMap(Map<String, Integer> map) {
        this.map = map;
    }

    public void setPropValue(Integer propValue) {
        this.propValue = propValue;
    }
}
